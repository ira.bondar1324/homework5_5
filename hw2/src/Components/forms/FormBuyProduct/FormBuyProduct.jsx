import {Form,Formik} from "formik";
import Input from "../../Input/Input";
import { object,string,number } from "yup";
import styles from "./FormBuyProduct.module.scss";
import { useSelector,useDispatch } from "react-redux";
import { setCountCart,setCart } from "../../../redux/cart/actionCreators";

const validationSchema=object({
    name:string().required("Name is required").min(2,"Must be more than 1 characters"),
    lastName:string().required("Last name is required").min(4,"Must be more than 3 characters"),
    age:number().required("Age is required").min(10,"Minimal age is 10").max(100,"Max age is 100"),
    adress:string().required("Address is required").min(10,"Must be more than 9 characters"),
    phone:number().required("Phone is required").min(10,"MUst be more than 9 characters").integer("Phone can't include a decimal point"),
})

const FormBuyProduct=()=>{

    const dispatch=useDispatch();
    const products=useSelector(state=>state.cart.cart)
    const initialValues={
        name:'',
        lastName:"",
        age:"",
        adress:'',
        phone:""
    }
    
    const onSubmit=(values,actions)=>{
        if(values){
            dispatch(setCountCart(0));
            dispatch(setCart([]));
            window.localStorage.setItem('countCart',JSON.stringify(0))
            window.localStorage.setItem('cart',JSON.stringify([]))
            console.log("Кошик: ",products, " Інформація з форми: ",values);
            actions.resetForm();
        }
    }

    return(
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            {({isValid})=>(
                <Form className={styles.form}>
                    <Input name="name" placeholder="Enter your name"/>
                    <Input name="lastName" placeholder="Enter your lastName"/>
                    <Input name="age" placeholder="Enter your age"/>
                    <Input name="adress" placeholder="Enter your adress"/>
                    <Input name="phone" placeholder="Enter your phone"/>

                    <button className={styles.btn}  disabled={!isValid} type="submit">Checkout</button>
                </Form>
            )}
        </Formik>
    )
}


export default FormBuyProduct;