import './App.css';
import Header from './Components/Header/Header';
import React, { useEffect} from 'react';
import AppRoutes from "./AppRoutes";
import { fetchItems,setItems} from './redux/items/actionCreators';
import { useDispatch, useSelector } from 'react-redux';

function App() {
  const dispatch=useDispatch();
  const itemsState=useSelector((state)=>state.favourite.favourite)


  useEffect(()=>{
      dispatch(fetchItems());
  },[])


  return (
    <>
      <Header/>
      <AppRoutes/>
    </>
  );
}

export default App;
