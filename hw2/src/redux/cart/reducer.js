import {ADD_ELEMENT_CART,DELETE_ELEMENT_CART,SET_CART,SET_COUNT_CART} from "./actions";
import { saveStateToLocalStorage,getStateFromLocalStorage } from "../../utils/localStorageHelper";

let initialValue={
    cart:[],
    countCart:0
}

if(getStateFromLocalStorage("cart")&&getStateFromLocalStorage("countCart")){

    initialValue={
        cart:getStateFromLocalStorage("cart"),
        countCart:getStateFromLocalStorage("countCart")
    }
}

const cartReducer = (state=initialValue,action)=>{
    switch(action.type){
        case (SET_CART):{
            return {...state, cart: action.payload }
        }
        case (SET_COUNT_CART):{
            return {...state, countCart: action.payload }
        }
        case (ADD_ELEMENT_CART):{
            const newCart=[...state.cart];
            let newCount=(state.countCart);
            const elem=action.payload;
            const index =newCart.findIndex((el)=>el.id===elem.id);
            if(index===-1){
                newCart.push(elem);
                newCount++;
                elem.cart=true;
                elem.countInCart=elem.countInCart+1;
                saveStateToLocalStorage("cart",newCart);
                saveStateToLocalStorage("countCart",newCount);
            }else{
                elem.countInCart=elem.countInCart+1;
            }
            return {...state,cart:newCart,countCart:newCount}
        }
        case (DELETE_ELEMENT_CART):{
            const newCart=[...state.cart];
            let newCount=(state.countCart);
            const elem=action.payload;
            const index=newCart.findIndex(el=>el.id===elem.id);
            elem.cart=false;
            elem.countInCart=0;
            newCart.splice(index,1);
            newCount--;
            saveStateToLocalStorage("cart",newCart);
            saveStateToLocalStorage("countCart",newCount);
            return {...state,cart:newCart,countCart:newCount}
        }

        default: return state;
    }
}

export default cartReducer;