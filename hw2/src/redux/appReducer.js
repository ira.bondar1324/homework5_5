import { combineReducers } from "redux";
import itemsReducer from './items/reducer';
import modalReducer from "./modal/reducer";
import cartReducer from "./cart/reducer";
import favouriteReducer from "./favourites/reducer";

const appReducer=combineReducers({
    items: itemsReducer,
    modal: modalReducer,
    cart:cartReducer,
    favourite:favouriteReducer
})
export default appReducer;