import {ADD_ELEMENT_FAVOURITE,DELETE_ELEMENT_FAVOURITE,SET_FAVOURITE} from "./actions";

export const addFavouriteElement = (elem)=>({type:ADD_ELEMENT_FAVOURITE, payload:elem});
export const deleteFavouriteElement = (elem)=>({type:DELETE_ELEMENT_FAVOURITE, payload:elem});
export const setFavourire = (favourite) => ({ type: SET_FAVOURITE, payload: favourite });



export const fetchFavourite = () => {
    return async (dispatch, getState) => {
        try {
            const favourites=localStorage.getItem("favourites");

            if(favourites){
                dispatch(setFavourire(favourites))
            }else{
                const favourite = getState().favourite;
                localStorage.setItem('favourites', JSON.stringify(favourite.favourite))
            }
        } catch (err) {
            console.log(err.message);
        }
    }
}

export const addFavourite = (item) => {
    return async (dispatch, getState) => {
        try {
            dispatch(addFavouriteElement(item));
            const favourite = getState().favourite;
            localStorage.setItem('favourites', JSON.stringify(favourite.favourite))
        } catch (err) {
            console.log(err.message);
        }
    }
}


export const deleteFavourite = (item) => {
    return async (dispatch, getState) => {
        try {
            dispatch(deleteFavouriteElement(item));
            const favourite = getState().favourite;
            localStorage.setItem('favourites', JSON.stringify(favourite.favourite))
        } catch (err) {
            console.log(err.message);
        }
    }
}