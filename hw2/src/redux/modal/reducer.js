import { TOGGLE_MODAL, TYPE_MODAL } from "./actions";

const initialValue={
    isOpen:false,
    type:null
}

const modalReducer = (state=initialValue,action)=>{
    switch(action.type){
        case (TOGGLE_MODAL):{
            return {...state,isOpen:action.payload}
        }
        case (TYPE_MODAL):{
            return {...state,type:action.payload}
        }
        default: return state;
    }
}

export default modalReducer;