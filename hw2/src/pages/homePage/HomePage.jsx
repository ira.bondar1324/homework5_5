import React from "react";
import ProductList from "../../Components/ProductList/ProductList";
import Modal from "../../Components/Modal/Modal";

const HomePage=()=>{
    return(
        <>
            <ProductList type='home' />
            <Modal type='home'/>
        </>
    );
}


export default HomePage;